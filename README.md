Run: yarn (or npm install)

Go to src/utils/getWeb3
  - change YOUR_KEY_HERE to infura key, or use your own private local chain

Go to src/server
  - run: node server.js

Go to src
  - run: npm start
