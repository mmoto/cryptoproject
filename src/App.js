import React, {Component} from 'react'
import getWeb3 from './utils/getWeb3'
import get from 'lodash/get'
import Web3 from 'web3'
import axios from 'axios'
import kittyAbi from './KittyABI';
import {Line} from 'rc-progress';
import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'
import 'rc-progress/assets/index.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.increase = this.increase.bind(this);
    this.restart = this.restart.bind(this);
    this.state = {
      web3: null,
      web3Socket: null,
      account: null,
      tx: null,
      display: null,
      ckAddress: "0x06012c8cf97BEaD5deAe237070F9587f8E7A266d",
      ckContract: null,
      targetAddress: null,
      tokenId: null,
      balance: null,
      tokens: null,
      inProgess: false,
      percent: 0
    }
  }

  componentDidMount() {
    getWeb3.then(results => {
      this.setState({
        web3: get(results, "web3")
      })
      // Instantiate contract once web3 provided.
      this.instantiateContract(get(results, "web3"))
    }).catch((e) => {
      console.log('Error finding web3.', e)
    })
    this.increase()
  }

  componentWillUnmount() {
    this.state.web3socket.unsubscribe()
  }

  instantiateContract(web3) {
    var self = this;
    /* Instantiate contract using HttpProvider */
    const ckContract = new web3.eth.Contract(kittyAbi, "0x06012c8cf97BEaD5deAe237070F9587f8E7A266d");
    this.setState({ckContract: ckContract});
    /* Initiate WebsocketProvider */
    const web3socket = new Web3(new Web3.providers.WebsocketProvider('wss://mainnet.infura.io/ws'))
    this.setState({web3socket: web3socket});
    this.watchPendingEvents(web3socket);
    /* Get accounts.*/
    web3.eth.getAccounts((error, accounts) => {
      if (error)
        return console.log(error)
      this.setState({
        account: get(accounts, '[0]')
      })
      /* Instantiate contract using WebsocketProvider */
      const ckSocket = new web3socket.eth.Contract(kittyAbi, "0x06012c8cf97BEaD5deAe237070F9587f8E7A266d");
      self.watchTransferEvents(ckSocket, get(accounts, '[0]'))
    })
  }

  /* Watch for pending events through websocket connected to mainnet */
  watchPendingEvents(socket) {
    var self = this;
    socket.eth.subscribe('pendingTransactions').on("data", function(txhash) {
      if (txhash && txhash.trim() == get(self.state, 'tx.txHash')) {
        let tx = {
          txHash: txhash,
          status: 'pending',
          time: Date.now()
        };
        self.setState({tx})
        self.sendRequest(tx)
      }
    })
  }

  /* Watch for transfer events on Kitty Contract from metamask address */
  watchTransferEvents(socket, account) {
    var self = this;
    socket.events.Transfer(function(err, event) {
      if (err)
        return console.log('Transfer Event Error: ', err);
      let watchAddress = get(event, 'returnValues.from')
      if (watchAddress && watchAddress.trim() === account.trim()) {
        let tx = {
          txHash: get(event, 'transactionHash'),
          from: get(event, 'returnValues.from'),
          to: get(event, 'returnValues.to'),
          status: 'complete',
          time: Date.now()
        };
        self.setState({tx: tx, inProgress: false})
        self.sendRequest(tx)
      }
    })
  }

  gift(e) {
    e.preventDefault();
    if (!this.state.targetAddress || !this.state.tokenId) {
      this.setState({display: 'Please enter an address and token id.'})
      return console.log('Please enter an address and token id.')
    } else if (!Web3.utils.isAddress(this.state.targetAddress)) {
      this.setState({display: 'Your address is not valid.'})
      return console.log('Your address is not valid.')
    }
    var self = this;
    var signer = this.state.account;
    var original_message = "Message here.";
    var message = "0x" + Web3.utils.toHex(original_message);
    var signature;
    this.state.web3.eth.personal.sign(message, signer, function(err, res) {
      if (err) {
        this.setState({display: 'Error signing transaction.'})
        return console.error('eth.personal.sign error: ', err);
      }
      signature = res;
      self.state.web3.eth.sendTransaction({
        gas: 300000,
        from: self.state.account,
        to: self.state.ckAddress,
        data: self.state.ckContract.methods.transfer(self.state.targetAddress, self.state.tokenId).encodeABI()
      }, 'latest').on('transactionHash', function(txHash) {
        let tx = {
          txHash: txHash.trim(),
          status: 'submitted',
          time: Date.now()
        };
        self.setState({tx: tx, inProgress: true});
      }).on('error', function(error) {
        console.log('Transaction Error: ', error)
        let tx = {
          txHash: get(this.state, 'tx'),
          status: 'error',
          time: Date.now()
        };
        self.setState({tx: tx, inProgress: false})
        self.sendRequest(tx)
      });
    });
  }

  balanceOf() {
    var self = this;
    if (!this.state.targetAddress) {
      this.setState({display: 'Please enter an address.'})
      return console.log('Please enter an address.')
    } else if (!Web3.utils.isAddress(this.state.targetAddress)) {
      this.setState({display: 'Your address is not valid.'})
      return console.log('Your address is not valid.')
    }
    this.state.web3.eth.call({
      gas: 300000,
      from: this.state.account,
      to: this.state.ckAddress,
      data: this.state.ckContract.methods.balanceOf(this.state.targetAddress).encodeABI()
    }, 'latest').then(function(res) {
      let balance = self.state.web3.utils.toDecimal(res).toFixed();
      self.setState({balance: balance})
    })
  }

  tokensOfOwner() {
    var self = this;
    if (!this.state.targetAddress) {
      this.setState({display: 'Please enter an address.'})
      return console.log('Please enter an address.')
    } else if (!Web3.utils.isAddress(this.state.targetAddress)) {
      this.setState({display: 'Your address is not valid.'})
      return console.log('Your address is not valid.')
    }
    this.state.web3.eth.call({
      gas: 300000,
      from: this.state.account,
      to: this.state.ckAddress,
      data: this.state.ckContract.methods.tokensOfOwner(this.state.targetAddress).encodeABI()
    }, 'latest').then(function(res) {
      console.log('tokensOfOwner: ', res)
      self.setState({tokens: res})
    })
  }

  sendRequest(transaction) {
    var self = this;
    axios.post("http://localhost:4001/transactions", {
      txHash: transaction.txHash,
      status: transaction.status,
      from: get(transaction, 'from'),
      to: get(transaction, 'to'),
      time: Date.now().toString()
    }).catch(function(error) {
      self.setState({display: 'Request timed out.'})
    });
  }

  increase() {
    const percent = this.state.percent + 1;
    if (percent >= 100) {
      clearTimeout(this.tm);
      this.restart()
      return;
    }
    this.setState({percent});
    this.tm = setTimeout(this.increase, 20);
  }

  restart() {
    clearTimeout(this.tm);
    this.setState({
      percent: 0
    }, () => {
      this.increase();
    });
  }

  render() {
    return (
      <div className="App">
        <nav className="navbar pure-menu pure-menu-horizontal">
          <a href="#" className="pure-menu-heading pure-menu-link">Mitsuaki</a>
        </nav>
        <main className="container">
          <div className="pure-g">
            <div className="pure-u-1-1">
              <p style={{
                fontFamily: "Arial"
              }}>Current Address: {this.state.account}
              </p>
              <div className="pure-u-1-1" style={{
                fontFamily: "Oswald"
              }}>Token Balance: {this.state.balance}</div>
              <div className="pure-form">
                <input className="pure-u-1-1" type="text" name="targetAddress" id="tokenId" placeholder="target address" onChange={e => this.setState({targetAddress: e.target.value})}/>
                <input className="pure-u-1-1" type="text" name="tokenId" id="tokenId" placeholder="token ID" onChange={e => this.setState({tokenId: e.target.value})}/>
                <button className="pure-button" onClick={this.balanceOf.bind(this)} style={{
                  margin: 10
                }}>balanceOf</button>
                <button className="pure-button" onClick={this.tokensOfOwner.bind(this)} style={{
                  margin: 10
                }}>
                  tokensOfOwner</button>
                <button className="pure-button" onClick={this.gift.bind(this)} style={{
                  margin: 10
                }}>
                  Gift Token</button>
              </div>
              <div style={{
                margin: 10,
                width: 150,
                display: this.state.inProgress
                  ? 'block'
                  : 'none'
              }}>
                <Line strokeWidth="4" percent={this.state.percent}/>
              </div>
              <table className="pure-table pure-table-bordered" style={{
                margin: 10,
                fontSize: 12
              }}>
                <thead>
                  <tr>
                    <th>Transaction</th>
                    <th>Status</th>
                    <th>Timestamp</th>
                    <th>From</th>
                    <th>To</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      {get(this.state, 'tx.txHash')}
                    </td>
                    <td>
                      {get(this.state, 'tx.status')}
                    </td>
                    <td>
                      {get(this.state, 'tx.time')}
                    </td>
                    <td>
                      {get(this.state, 'tx.from')}
                    </td>
                    <td>
                      {get(this.state, 'tx.to')}
                    </td>
                  </tr>
                </tbody>
              </table>
              <div className="pure-u-1-1">{get(this.state, 'display')}
              </div>
              <div className="pure-u-1-1">
                {this.state.tokens && Array.isArray(this.state.tokens)
                  ? this.state.tokens.map((token, i) => {
                    return (
                      <div key={i} className="pure-u-1-1">{token}</div>
                    )
                  })
                  : null
                }
              </div>
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default App
