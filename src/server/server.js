const express = require('express')
const http = require("http")
const cors = require('cors');
const bodyParser = require('body-parser')
const low = require('lowdb')
const port = process.env.PORT || 4001;
const FileAsync = require('lowdb/adapters/FileAsync')

// Create server
const app = express()
app.use(cors());

app.use(bodyParser.json())
const server = http.createServer(app);

// Create database instance and start server
const adapter = new FileAsync('db.json')
low(adapter)
  .then(db => {
    // Routes
    // GET /transactions
    app.get('/transactions', (req, res) => {
      const txs = db.get('transactions')
        .value()
      res.send(txs)
    })
    // GET /posts/:id
    app.get('/transactions/:txhash', (req, res) => {
      const tx = db.get('transactions')
        .find({ txhash: req.params.txhash })
        .value()
      res.send(tx)
    })
    // POST /posts
    app.post('/transactions', (req, res) => {
      db.get('transactions')
        .push(req.body)
        .last()
        .write()
        .then(post => res.send(post))
    })
    // Set db default values
    return db.defaults({ transactions: [] }).write()
  })
  .then(() => {
    server.listen(port, () => console.log('listening on port 4001'))
  })
